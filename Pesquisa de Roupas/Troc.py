# -*- coding: utf-8 -*-
from __future__ import print_function
from bs4 import BeautifulSoup
import requests
import os
import csv
import re
from xml.dom.minidom import parse
import xml.dom.minidom
import datetime
import sys  
from random import randrange


reload(sys)  
sys.setdefaultencoding('utf-8')

def writeResults(listaPecas): 

    arquivoOutput = path + 'Troc.csv'

    with open(arquivoOutput, 'wb') as csvfile:

        spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)

        spamwriter.writerow(['id', 'link', 'marca', 'nomeDaPeca', 'precoComDesconto', 'precoSemDesconto', 'Descricao', 'Fotos', 'Tamanho', 'Condicao', 'Cores', 'Disponível', 'Data', 'CodigoVerificacao', 'Status', 'Palavra-Chave1', 'Palavra-Chave2','Palavra-Chave3','Palavra-Chave4','Palavra-Chave5','Palavra-Chave6','Palavra-Chave7', 'Palavra-Chave8' ])
        
        for peca in listaPecas:

            spamwriter.writerow(peca)

def writeFileHTML(random_id, content):

    arquivoOutput = 'Resultados/Troc/troc_' + str(random_id) +  '.html'

    with open(arquivoOutput, "wb") as text_file:
        text_file.write(content)

quantidadeTotalVerificacao = 101006

listaPecas = []

# create directory
path = r'Resultados/'

if not os.path.isdir(path):
    os.makedirs(path)

if not os.path.isdir(path + 'Troc/'):
    os.makedirs(path + 'Troc/')

arquivoInput = path + 'Troc.csv'

mode = 'r' if os.path.exists(arquivoInput) else 'a+'
    
with open(arquivoInput, mode) as csvFile:
    reader = csv.reader(csvFile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    listaPecas = list(reader)

csvFile.close()

if len(listaPecas) > 0:
    del listaPecas[0]

chavesNaoVerificadas = []
chavesVerificadas = []

for peca in listaPecas:
    chavesVerificadas.append(int(peca[0]))

    # tratamento

    # peca[4] = ((((peca[4].lower().replace('por:', '')).replace('r$', '')).replace('.','')).replace(',','.').strip())
    # peca[5] = ((((peca[5].lower().replace('por:', '')).replace('r$', '')).replace('.','')).replace(',','.').strip())

    # if peca[4] == '':
    #     peca[4] = '0'
    # if peca[5] == '':
    #     peca[5] = '0'
    
    # peca[4] =  float(re.sub(r'[^0-9\.]', '', peca[4]))
    # peca[5] =  float(re.sub(r'[^0-9\.]', '', peca[5]))


    # print(str(peca[0]) + ' --- '+ str(peca[4]) +' --- '+ str(peca[5]))

    palavras  = peca[3].strip().split(" ")

    for i in range(0, 8):
        if i < len(palavras): 
            peca.append(palavras[i])
        else: 
            peca.append(' ')
    

print('CHAVES VERIFICADAS ' + str(len(chavesVerificadas)))
print('VERIFICANDO CHAVES')

s = set(chavesVerificadas)
chavesNaoVerificadas = [x for x in range(0,quantidadeTotalVerificacao) if x not in s]
       
while(len(chavesNaoVerificadas) > 0):

    random = randrange(len(chavesNaoVerificadas))

    page_link = 'https://troc.com.br/produto/' + str(chavesNaoVerificadas[random])

    print(page_link, end='')

    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

    page_response = requests.get(page_link, headers=headers)

    print(' ' + str(page_response))

    page_content = BeautifulSoup(page_response.content, "html.parser")

    # writeFileHTML(random, page_response.content)

    div = page_content.find_all('div', {"class": "single-para simpleCart_shelfItem"})

    peca = []

    marca = ""
    nomeDaPeca = ""
    precoComDesconto = ""
    precoSemDesconto = ""
    descricao = ""
    fotos = [] 
    tamanho = ""
    condicao = ""
    cores = ""
    disponivel = ""
    status = "VERIFICADO"

    if len(div) > 0:

        marca = div[0].h2.text
        nomeDaPeca = div[0].h3.text
        precoComDesconto = div[0].h4.text
        precoSemDesconto = div[0].find_all('h6', {"class": "price price_old"})
        descricao = div[0].find_all('p', {'class': "product-description"})[0].text
        disponivel = div[0].find_all('h4', {'class': 'status-sold'})

        if len(precoSemDesconto) > 0: 
            precoSemDesconto = div[0].find_all('h6', {"class": "price price_old"})[0].text
        else: 
            precoSemDesconto = ""

        if len(disponivel) > 0:
            disponivel = div[0].find_all('h4', {'class': 'status-sold'})[0].text
        else:
            disponivel = "DISPONIVEL"

        li = div[0].find_all('li')

        for l in li:

            span = l.find_all('span')
            span0 = (span[0].text).lower()

            if 'tamanho' in span0:
                tamanho = span[1].text

            if 'condição' in span0:
                condicao = span[1].text

            if 'cores' in span0:
                cores = span[1].text

        imgsDivs = page_content.find_all('div', {'class': 'carousel-inner'}) 
         
        imgsDiv = imgsDivs[0].find_all('div')

        for imgDiv in imgsDiv:
            fotos.append(imgDiv.img['src'].encode("utf-8"))
           
    else:
        status = "PAGINA_NAO_DISPONIVEL"
    
    precoComDesconto = ((((precoComDesconto.lower().replace('por:', '')).replace('r$', '')).replace('.','')).replace(',','.').strip())
    precoSemDesconto = ((((precoSemDesconto.lower().replace('por:', '')).replace('r$', '')).replace('.','')).replace(',','.').strip())

    if precoComDesconto == '':
        precoComDesconto = '0'
    if precoSemDesconto == '':
        precoSemDesconto = '0'

    precoComDesconto =  float(re.sub(r'[^0-9\.]', '', precoComDesconto))
    precoSemDesconto =  float(re.sub(r'[^0-9\.]', '', precoSemDesconto))

    peca.append(chavesNaoVerificadas[random])
    peca.append(page_link)
    peca.append(marca)
    peca.append(nomeDaPeca)
    peca.append((precoComDesconto))
    peca.append((precoSemDesconto))
    peca.append(descricao)
    peca.append(fotos)
    peca.append(tamanho)
    peca.append(condicao)
    peca.append(cores)
    peca.append(disponivel)
    peca.append(datetime.datetime.now())
    peca.append("PRI-TROC")
    peca.append(status)

    palavras  = peca[3].strip().split(" ")

    for i in range(0, 8):
        if i < len(palavras): 
            peca.append(palavras[i])
        else: 
            peca.append(' ')

    listaPecas.append(peca)

    qtdChavesNaoVerificadas = len(chavesNaoVerificadas)

    if qtdChavesNaoVerificadas%30 == 0 or qtdChavesNaoVerificadas < 30:
        writeResults(listaPecas)
        print("SAVING RESULTS")
        print("TOTAL " + str(quantidadeTotalVerificacao - qtdChavesNaoVerificadas))

    del chavesNaoVerificadas[random]