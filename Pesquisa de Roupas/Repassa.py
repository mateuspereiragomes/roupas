# -*- coding: utf-8 -*-
from __future__ import print_function
from bs4 import BeautifulSoup
import requests
import os
import csv
import re
from xml.dom.minidom import parse
import xml.dom.minidom
import datetime
import sys  
from random import randrange


reload(sys)  
sys.setdefaultencoding('utf-8')

def writeResults(listaPecas): 

    arquivoOutput = path + 'Repassa.csv'

    with open(arquivoOutput, 'wb') as csvfile:

        spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)

        spamwriter.writerow(['id', 'link', 'marca', 'nomeDaPeca', 'tags', 'precoComDesconto', 'precoSemDesconto', 'Descricao', 'Fotos', 'Tamanho', 'Condicao', 'Cores', 'Composicao' , 'Medidas', 'Disponível', 'Data', 'CodigoVerificacao', 'Status', 'Palavra-Chave1', 'Palavra-Chave2','Palavra-Chave3','Palavra-Chave4','Palavra-Chave5','Palavra-Chave6','Palavra-Chave7', 'Palavra-Chave8'])
        
        for peca in listaPecas:

            spamwriter.writerow(peca)

quantidadeTotalVerificacao = 83000

listaPecas = []

# create directory
path = r'Resultados/'

if not os.path.isdir(path):
    os.makedirs(path)

if not os.path.isdir(path + 'Repassa/'):
    os.makedirs(path + 'Repassa/')

arquivoInput = path + 'Repassa.csv'

mode = 'r' if os.path.exists(arquivoInput) else 'a+'
    
with open(arquivoInput, mode) as csvFile:
    reader = csv.reader(csvFile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    listaPecas = list(reader)

csvFile.close()

if len(listaPecas) > 0:
    del listaPecas[0]

chavesNaoVerificadas = []
chavesVerificadas = []

for peca in listaPecas:
    chavesVerificadas.append(int(peca[0]))

print('CHAVES VERIFICADAS ' + str(len(chavesVerificadas)))
print('VERIFICANDO CHAVES')

s = set(chavesVerificadas)
chavesNaoVerificadas = [x for x in range(0,quantidadeTotalVerificacao) if x not in s]
       
while(len(chavesNaoVerificadas) > 0):

    random = randrange(len(chavesNaoVerificadas))

    page_link = 'https://www.repassa.com.br/produtos/' + str(chavesNaoVerificadas[random])

    print(page_link, end='')

    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

    page_response = requests.get(page_link, headers=headers)

    print(' ' + str(page_response), end='')

    page_content = BeautifulSoup(page_response.content, "html.parser")

    peca = []

    marca = ""
    nomeDaPeca = ""
    precoComDesconto = ""
    precoSemDesconto = ""
    descricao = ""
    fotos = [] 
    tamanho = ""
    condicao = ""
    cores = ""
    disponivel = ""
    status = "VERIFICADO"
    composicao = ""
    medidas = ""
    tags = ""

    main = page_content.find_all('main')

    if len(main)>0:
        main = main[0]
    
    infoProduto = main.find_all('div', {'class': 'info-produto'})

    if len(infoProduto) > 0:

        print(' RESULTADO ', end='')

        infoProduto = infoProduto[0]

        tituloGlobal = main.find('div', {"class": 'titulo-global'})

        nomeDaPeca = tituloGlobal.find_all('h2')[0].text

        tags = [tag.text.replace('/', '').strip() for tag in tituloGlobal.find_all('li')]

        boxInfo = infoProduto.find_all('div', {'class': 'box-info'})

        for info in boxInfo:
            resultado = info.text.strip().split('\n')
            if len(resultado) > 1:
                if 'marca' in resultado[0].lower():
                    marca = resultado[1]
                if 'tamanho' in resultado[0].lower():
                    tamanho = resultado[1]
                if 'estado' in resultado[0].lower():
                    condicao = resultado[1]

        precoComDesconto = infoProduto.find('div', {'class': 'product-price'}).text
        precoComDesconto = precoComDesconto.lower().replace('r$','').replace('.','').replace(',','.').split('-')
        precoSemDesconto = float(precoComDesconto[1].strip())
        precoComDesconto = float(precoComDesconto[0].strip())

        descricao = page_content.find('div', {'class': 'row border-vertical mt_5 mb_5 icons-line'}).find_next('div', {'class': 'row'})
        descricao.find('div', {'class': 'titulo-global'}).extract()
        ps = descricao.find_all('p')
        if len(ps) > 0:
                
            for br in ps[1].find_all('br'):
                br.replace_with('\n')

            informacoes = ps[1].text.split('\n')
            descricao = informacoes[0]
            for informacao in informacoes:
                prov = informacao.lower()
                if 'medidas' in prov:
                    medidas =  informacao.replace('Medidas:', '').strip()
                if 'composição' in prov:
                    composicao =  informacao.replace('Composição:', '').strip()
                if 'cor' in prov:
                    cores =  informacao.replace('Cor:', '').strip()

        fotos = [x['src'] for x in main.find('div', {'class': 'carrousel-fotos-produto-mobile'}).find_all('img')]

        disponivel = main.find_all('button', {'class':'comprar iwant'})
        if len(disponivel) > 0:
            disponivel = 'DISPONIVEL'
        else:
            disponivel = 'VENDIDO'
        print(disponivel)

    else:
        status = "PAGINA_NAO_DISPONIVEL"
        print('')

    if precoComDesconto == '':
        precoComDesconto = '0'
    if precoSemDesconto == '':
        precoSemDesconto = '0'
    
    peca.append(chavesNaoVerificadas[random])
    peca.append(page_link)
    peca.append(marca)
    peca.append(nomeDaPeca)
    peca.append(tags)
    peca.append(float(precoComDesconto))
    peca.append(float(precoSemDesconto))
    peca.append(descricao)
    peca.append(fotos)
    peca.append(tamanho)
    peca.append(condicao)
    peca.append(cores)
    peca.append(composicao)
    peca.append(medidas)
    peca.append(disponivel)
    peca.append(datetime.datetime.now())
    peca.append("PRI-REPASSA")
    peca.append(status)

    palavras  = peca[3].strip().split(" ")

    for i in range(0, 8):
        if i < len(palavras): 
            peca.append(palavras[i])
        else: 
            peca.append(' ')

    listaPecas.append(peca)

    qtdChavesNaoVerificadas = len(chavesNaoVerificadas)

    if qtdChavesNaoVerificadas%10 == 0 or qtdChavesNaoVerificadas < 10:
        writeResults(listaPecas)
        print("SAVING RESULTS")
        print("TOTAL " + str(quantidadeTotalVerificacao - qtdChavesNaoVerificadas))

    del chavesNaoVerificadas[random]