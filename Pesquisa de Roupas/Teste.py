 # -*- coding: utf-8 -*-
from __future__ import print_function
from bs4 import BeautifulSoup
import requests
import os
import csv
import re
from xml.dom.minidom import parse
import xml.dom.minidom
import datetime
import sys  
from random import randrange

reload(sys)  
sys.setdefaultencoding('utf-8')

# Buscar elementos para uma única págin

page_link = 'https://www.repassa.com.br/produtos/57067'

print(page_link, end='')

headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

page_response = requests.get(page_link, headers=headers)

print(' ' + str(page_response))

page_content = BeautifulSoup(page_response.content, "html.parser")

peca = []

marca = ""
nomeDaPeca = ""
precoComDesconto = ""
precoSemDesconto = ""
descricao = ""
fotos = [] 
tamanho = ""
condicao = ""
cores = ""
disponivel = ""
status = "VERIFICADO"
tags = []

main = page_content.find_all('main')[0]

tituloGlobal = main.find('div', {"class": 'titulo-global'})

nomeDaPeca = tituloGlobal.find_all('h2')[0].text

tags = [tag.text.replace('/', '').strip() for tag in tituloGlobal.find_all('li')]

infoProduto = main.find('div', {'class': 'info-produto'})

boxInfo = infoProduto.find_all('div', {'class': 'box-info'})

for info in boxInfo:
    resultado = info.text.strip().split('\n')
    if 'marca' in resultado[0].lower():
        marca = resultado[1]
    if 'tamanho' in resultado[0].lower():
        tamanho = resultado[1]
    if 'estado' in resultado[0].lower():
        estado = resultado[1]

precoComDesconto = infoProduto.find('div', {'class': 'product-price'}).text
precoComDesconto = precoComDesconto.lower().replace('r$','').replace('.','').replace(',','.').split('-')
precoSemDesconto = float(precoComDesconto[1].strip())
precoComDesconto = float(precoComDesconto[0].strip())

descricao = page_content.find('div', {'class': 'row border-vertical mt_5 mb_5 icons-line'}).find_next('div', {'class': 'row'})
descricao.find('div', {'class': 'titulo-global'}).extract()
ps = descricao.find_all('p')
if len(ps) > 0:
        
    for br in ps[1].find_all('br'):
        br.replace_with('\n')

    informacoes = ps[1].text.split('\n')
    descricao = informacoes[0]
    for informacao in informacoes:
        prov = informacao.lower()
        if 'medidas' in prov:
            medidas =  informacao.replace('Medidas:', '').strip()
        if 'composição' in prov:
            composicao =  informacao.replace('Composição:', '').strip()
        if 'cor' in prov:
            cor =  informacao.replace('Cor:', '').strip()

fotos = [x['src'] for x in main.find('div', {'class': 'carrousel-fotos-produto-mobile'}).find_all('img')]

disponivel = main.find('button', {'class':'comprar iwant'})
if len(disponivel) > 0:
    disponivel = 'DISPONIVEL'
else:
    disponivel = 'VENDIDO'

