# coding: utf8
from __future__ import print_function
from bs4 import BeautifulSoup
import requests
import os
import csv
import re
from xml.dom.minidom import parse
import xml.dom.minidom
import datetime
import sys  
from random import randrange
import json

reload(sys)  
sys.setdefaultencoding('utf-8')

# //primeiro busca todas as chaves no site
# verifica se existe chaves no arquivo 
# faz a diferença delas

# grava chaves novas no arquivo 

# verifica chaves não buscadas
# faz random de chaves não buscadas

def writeResults(listaPecas): 

    arquivoOutput = path + 'Garimpario2.csv'

    with open(arquivoOutput, 'wb') as csvfile:

        spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)

        spamwriter.writerow(['id', 'link', 'marca', 'nomeDaPeca', 'precoComDesconto', 'precoSemDesconto', 'Descricao', 'Fotos', 'Tamanho', 'Condicao', 'Cores', 'Disponível', 'Data', 'CodigoVerificacao', 'Status', 'Tags' ])
        
        for peca in listaPecas:

            spamwriter.writerow(peca)


chavesSelecionadas = []
for i in range(1,1):

    page_link = 'https://www.garimpario.com.br/page/'+ str(i)+'/?s&action=wowmall_ajax_search&post_type=product'
    
    print(page_link, end='')

    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

    page_response = requests.get(page_link, headers=headers)

    print(' ' + str(page_response))

    page_content = BeautifulSoup(page_response.content, "html.parser")

    conteudo = page_content.find_all('a', {'class': 'ajax_add_to_cart'})

    chavesSelecionadas = list(set(chavesSelecionadas) | set(int(x['data-product_id']) for x in conteudo))

    print(chavesSelecionadas)
    print(len(chavesSelecionadas))


listaPecas = []

# create directory
path = r'Resultados/'

if not os.path.isdir(path):
    os.makedirs(path)

if not os.path.isdir(path + 'Garimpario/'):
    os.makedirs(path + 'Garimpario/')

arquivoInput = path + 'Garimpario2.csv'

mode = 'r' if os.path.exists(arquivoInput) else 'a+'
    
with open(arquivoInput, mode) as csvFile:
    reader = csv.reader(csvFile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    listaPecas = list(reader)

csvFile.close()

if len(listaPecas) > 0:
    del listaPecas[0]

chaves = []

for peca in listaPecas:
    chaves.append(int(peca[0]))

s = set(chaves)
chavesNaoIncluidas = [x for x in chavesSelecionadas if x not in s]

for chave in chavesNaoIncluidas:

    peca = []

    marca = " "
    nomeDaPeca = " "
    precoComDesconto = " "
    precoSemDesconto = " "
    descricao = " "
    fotos = [] 
    tamanho = " "
    condicao = " "
    cores = " "
    disponivel = " "
    status = "LISTADO"
    tags = [] 

    peca.append(int(chave))
    peca.append(' ')#page_link)
    peca.append(marca)
    peca.append(nomeDaPeca)
    peca.append(precoComDesconto)
    peca.append(precoSemDesconto)
    peca.append(descricao)
    peca.append(fotos)
    peca.append(tamanho)
    peca.append(condicao)
    peca.append(cores)
    peca.append(disponivel)
    peca.append(datetime.datetime.now())
    peca.append("PRI-GAR")
    peca.append(status)
    peca.append(tags)

    listaPecas.append(peca)

writeResults(listaPecas)

chaves = []
chavesVerificadas = []
chavesNaoVerificadas = []

for peca in listaPecas:
    chaves.append(int(peca[0]))
    if 'LISTADO' in peca[14]:
        chavesNaoVerificadas.append(int(peca[0]))

print('QTDE NAO VERIFICADO ' + str(len(chavesNaoVerificadas)))
        
while(len(chavesNaoVerificadas) > 0):

    random = randrange(len(chavesNaoVerificadas))

    page_link = 'https://www.garimpario.com.br?p='+ str(chavesNaoVerificadas[random])

    print(page_link, end='')

    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

    page_response = requests.get(page_link, headers=headers)

    print(' ' + str(page_response), end='')

    page_content = BeautifulSoup(page_response.content, "html.parser")

    product_id = 'product-' + str(chavesNaoVerificadas[random])

    conteudo = page_content.find_all('div', {'id': product_id})

    peca = []

    marca = ""
    nomeDaPeca = ""
    precoComDesconto = ""
    precoSemDesconto = ""
    descricao = ""
    fotos = [] 
    tamanho = ""
    condicao = ""
    cores = ""
    disponivel = ""
    status = "VERIFICADO"
    tags = [] 

    if len(conteudo) > 0:

        print('validada')

        conteudo = conteudo[0]

        nomeDaPeca = conteudo.find_all('h1', {"class": "product_title entry-title"})
    
        if len(nomeDaPeca)> 0:
            nomeDaPeca = nomeDaPeca[0].text
        else:
            nomeDaPeca = ''

        descricao = conteudo.find_all('div', {"class": "woocommerce-product-details__short-description"})

        if len(descricao)> 0:
            descricao = descricao[0].p.text
        else:
            descricao = ''
        
        if conteudo.find('span', {"class": "tagged_as"}) != None :
            lista1 = set(x.text for x in conteudo.find('span', {"class": "tagged_as"}).find_all('a'))
        else: 
            lista1 = set([])
        
        if len(conteudo.find_all('div', {"class": "loop-product-categories"})) > 0:
            lista2 = set(x.text for x in conteudo.find_all('div', {"class": "loop-product-categories"})[0].find_all('a'))
        else:
            lista2 = set([])

        tags = list( lista1 | lista2 )

        table = conteudo.find_all("table", {"class": "shop_attributes"})

        if len(table) > 0: 

            trs = table[0].find_all("tr")

            for tr in trs:
                tds = tr.find_all('td')
                for i in range(0,len(tds),2):
                    primeiro = tds[i].text.lower().encode('utf-8')
                    segundo = tds[i+1].text.strip().encode('utf-8')
                    if 'tamanho' in primeiro:
                        tamanho = segundo
                    if 'marca' in primeiro:
                        marca = segundo
                    if 'material' in primeiro:
                        material = segundo 
                    if 'condição' in primeiro:
                        condicao = segundo
                    if 'medidas da peça' in primeiro:
                        medidas = segundo

        sku = conteudo.find('span', {"class": "sku"})

        if sku != None:
            sku = int(sku.text)
        else: 
            sku = ' '

        precoSemDesconto = conteudo.find('p', {'class', 'price'})
   
        if len(precoSemDesconto.find_all('ins')) > 0:
            precoComDesconto = precoSemDesconto.find('ins').text.replace('R$', '').strip()
            precoSemDesconto = precoSemDesconto.find('del').text.replace('R$', '').strip()
        else: 
            precoSemDesconto.find_all('span', {'class', 'wc-simulador-parcelas-parcelamento-info-container'})[0].extract()
            precoSemDesconto = int(precoSemDesconto.text.replace('R$', '').strip())
            precoComDesconto = precoSemDesconto

        fotos = [str(x['href']) for x in conteudo.find_all('a', {'class': 'zoom'})]

        disponivel = conteudo.find_all('p', {'class': 'stock in-stock'})

        if len(disponivel) == 0: 
            disponivel = 'VENDIDO'
        else: 
            disponivel = 'DISPONIVEL'

    else:
        status = "PAGINA_NAO_DISPONIVEL"
       
    for i in range(0,len(listaPecas)):
        if int(listaPecas[i][0]) == int(chavesNaoVerificadas[random]):
            listaPecas[i][1] = page_link
            listaPecas[i][2] = marca
            listaPecas[i][3] = nomeDaPeca
            listaPecas[i][4] = precoComDesconto
            listaPecas[i][5] = precoSemDesconto
            listaPecas[i][6] = descricao
            listaPecas[i][7] = fotos
            listaPecas[i][8] = tamanho
            listaPecas[i][9] = condicao
            listaPecas[i][10] = cores
            listaPecas[i][11] = disponivel
            listaPecas[i][12] = datetime.datetime.now()
            listaPecas[i][13] = "PRI-GAR"
            listaPecas[i][14] = status
            listaPecas[i][15] = tags


    qtdChavesNaoVerificadas = len(chavesNaoVerificadas)

    print('')

    if qtdChavesNaoVerificadas%10 == 0 or qtdChavesNaoVerificadas < 10:
        writeResults(listaPecas)
        print("SAVING RESULTS")
        print("TOTAL " + str(len(chaves) - qtdChavesNaoVerificadas))

    del chavesNaoVerificadas[random]

    
