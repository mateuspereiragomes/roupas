# coding: utf8
from __future__ import print_function
from bs4 import BeautifulSoup
import requests
import os
import csv
import re
from xml.dom.minidom import parse
import xml.dom.minidom
import datetime
import sys  
from random import randrange
import json

reload(sys)  
sys.setdefaultencoding('utf-8')

def writeResults(listaPecas): 

    arquivoOutput = path + 'Garimpario.csv'

    with open(arquivoOutput, 'wb') as csvfile:

        spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)

        spamwriter.writerow(['id', 'link', 'marca', 'nomeDaPeca', 'precoComDesconto', 'precoSemDesconto', 'Descricao', 'Fotos', 'Tamanho', 'Condicao', 'Cores', 'Disponível', 'Data', 'CodigoVerificacao', 'Status', 'Tags' ])
        
        for peca in listaPecas:

            spamwriter.writerow(peca)

listaPecas = []

# create directory
path = r'Resultados/'

if not os.path.isdir(path):
    os.makedirs(path)

if not os.path.isdir(path + 'Garimpario/'):
    os.makedirs(path + 'Garimpario/')

arquivoInput = path + 'Garimpario.csv'

mode = 'r' if os.path.exists(arquivoInput) else 'a+'
    
with open(arquivoInput, mode) as csvFile:
    reader = csv.reader(csvFile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    listaPecas = list(reader)

csvFile.close()

if len(listaPecas) > 0:
    del listaPecas[0]

chavesNaoVerificadas = []
chavesVerificadas = []
chaves = []

for peca in listaPecas:
    chaves.append(int(peca[0]))
    if 'VERIFICADO' in peca[15]:
        chavesVerificadas.append(int(peca[0]))
    else:
        chavesNaoVerificadas.append(int(peca[0]))

print('CHAVES VERIFICADAS ' + str(len(chavesVerificadas)))
print('CHAVES NÃO VERIFICADAS ' + str(len(chavesVerificadas)))
print('VERIFICANDO CHAVES')

chavesSelecionadas = []
for i in range(1,44):

    page_link = 'https://www.garimpario.com.br/page/'+ str(i)+'/?s&action=wowmall_ajax_search&post_type=product'
    
    print(page_link, end='')

    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

    page_response = requests.get(page_link, headers=headers)

    print(' ' + str(page_response))

    page_content = BeautifulSoup(page_response.content, "html.parser")

    conteudo = page_content.find_all('a', {'class': 'ajax_add_to_cart'})

    chavesSelecionadas = list(set(chavesSelecionadas) | set(int(x['data-product_id']) for x in conteudo))

    print(chavesSelecionadas)
    print(len(chavesSelecionadas))

s = set(chaves)
chavesNaoIncluidas = [x for x in chavesSelecionadas if x not in s]

       
while(len(chavesNaoVerificadas) > 0):

    random = randrange(len(chavesNaoVerificadas))

    page_link = 'https://www.garimpario.com.br?p='+ str(chavesNaoVerificadas[random])

    print(page_link, end='')

    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

    page_response = requests.get(page_link, headers=headers)

    print(' ' + str(page_response), end='')

    page_content = BeautifulSoup(page_response.content, "html.parser")

    # writeFileHTML(random, page_response.content)

    product_id = 'product-' + str(chavesNaoVerificadas[random])

    conteudo = page_content.find_all('div', {'id': product_id})

    peca = []

    marca = ""
    nomeDaPeca = ""
    precoComDesconto = ""
    precoSemDesconto = ""
    descricao = ""
    fotos = [] 
    tamanho = ""
    condicao = ""
    cores = ""
    disponivel = ""
    status = "VERIFICADO"
    tags = [] 

    if len(conteudo) > 0:

        print('validada')

        conteudo = conteudo[0]

        nomeDaPeca = conteudo.find_all('h1', {"class": "product_title entry-title"})[0].text

        descricao = conteudo.find_all('div', {"class": "woocommerce-product-details__short-description"})[0].p.text

        tags = list(set(x.text for x in conteudo.find('span', {"class": "tagged_as"}).find_all('a')) |  set(x.text for x in conteudo.find_all('div', {"class": "loop-product-categories"})[0].find_all('a')))
        
        # tags = list(set(x.text for x in ) |  set(x.text for x in 
        
        # for tag in conteudo.find('span', {"class": "tagged_as"}).find_all('a'):
        #     tags.append(tag.text.encode('utf-8'))
        
        # for tag in conteudo.find_all('div', {"class": "loop-product-categories"})[0].find_all('a'): 
        #     tags.append(tag.text.encode('utf-8'))

        table = conteudo.find_all("table", {"class": "shop_attributes"})

        trs = table[0].find_all("tr")

        for tr in trs:
            tds = tr.find_all('td')
            for i in range(0,len(tds),2):
                primeiro = tds[i].text.lower().encode('utf-8')
                segundo = tds[i+1].text.strip().encode('utf-8')
                if 'tamanho' in primeiro:
                    tamanho = segundo
                if 'marca' in primeiro:
                    marca = segundo
                if 'material' in primeiro:
                    material = segundo 
                if 'condição' in primeiro:
                    condicao = segundo
                if 'medidas da peça' in primeiro:
                    medidas = segundo

        sku = int(conteudo.find('span', {"class": "sku"}).text)

        precoSemDesconto = conteudo.find('p', {'class', 'price'})
   
        if len(precoSemDesconto.find_all('ins')) > 0:
            precoComDesconto = precoSemDesconto.find('ins').text.replace('R$', '').strip()
            precoSemDesconto = precoSemDesconto.find('del').text.replace('R$', '').strip()
        else: 
            precoSemDesconto.find_all('span', {'class', 'wc-simulador-parcelas-parcelamento-info-container'})[0].extract()
            precoSemDesconto = int(precoSemDesconto.text.replace('R$', '').strip())
            precoComDesconto = precoSemDesconto

        fotos = [str(x['href']) for x in conteudo.find_all('a', {'class': 'zoom'})]

        disponivel = conteudo.find_all('p', {'class': 'stock in-stock'})

        if len(disponivel) == 0: 
            disponivel = 'VENDIDO'
        else: 
            disponivel = 'DISPONIVEL'

    else:
        status = "PAGINA_NAO_DISPONIVEL"
       
    peca.append(chavesNaoVerificadas[random])
    peca.append(page_link)
    peca.append(marca)
    peca.append(nomeDaPeca)
    peca.append(precoComDesconto)
    peca.append(precoSemDesconto)
    peca.append(descricao)
    peca.append(fotos)
    peca.append(tamanho)
    peca.append(condicao)
    peca.append(cores)
    peca.append(disponivel)
    peca.append(datetime.datetime.now())
    peca.append("PRI-GAR")
    peca.append(status)
    peca.append(tags)

    listaPecas.append(peca)

    qtdChavesNaoVerificadas = len(chavesNaoVerificadas)

    print('')

    if qtdChavesNaoVerificadas%10 == 0 or qtdChavesNaoVerificadas < 10:
        writeResults(listaPecas)
        print("SAVING RESULTS")
        print("TOTAL " + str(len(chavesSelecionadas) - qtdChavesNaoVerificadas))

    del chavesNaoVerificadas[random]

    