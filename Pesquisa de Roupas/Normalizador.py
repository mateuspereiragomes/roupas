# coding: utf8
from __future__ import print_function
from bs4 import BeautifulSoup
import requests
import os
import csv
import re
from xml.dom.minidom import parse
import xml.dom.minidom
import datetime
import sys  
from random import randrange
import json

reload(sys)  
sys.setdefaultencoding('utf-8')


#fazer backup dos dados ok 
# dividir nomedapeca garimpario em palavras chaves 
# verificar quais os campos que existirão OK
# criar arquivo que carrega na memória cada arquivo 
# fazer correlação de colunas
# salvar arquivo resultado 


#abrir arquivos de cada loja
listaTroc = []
listaRepassa = []
listaGarimpario = []

# create directory
path = r'Resultados/'

if not os.path.isdir(path):
    os.makedirs(path)


# leitura troc

arquivoInput = path + 'Troc.csv'

mode = 'r' if os.path.exists(arquivoInput) else 'a+'
    
with open(arquivoInput, mode) as csvFile:
    reader = csv.reader(csvFile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    listaTroc = list(reader)


#leitura repassa

arquivoInput = path + 'Repassa.csv'

mode = 'r' if os.path.exists(arquivoInput) else 'a+'
    
with open(arquivoInput, mode) as csvFile:
    reader = csv.reader(csvFile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    listaRepassa = list(reader)


#leitura garimpario 

arquivoInput = path + 'Garimpario2.csv'

mode = 'r' if os.path.exists(arquivoInput) else 'a+'
    
with open(arquivoInput, mode) as csvFile:
    reader = csv.reader(csvFile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    listaGarimpario = list(reader)

# CABEÇALHO FINAL 
# 0 - idArquivo
# 1 - idLoja
# 2 - loja
# 3 - link 
# 4 - marca 
# 5 - nomeDaPeca
# ok

# 6 - precoComDesconto 
# 7 - precoSemDesconto
# 8- Descricao 
# 9 - Fotos
# 10 - tamanho 
# ok

# 11 - condicao 
# 12 - Cores
# 13 - Disponivel 
# ok

# 14 - data 
# 15 - CodigoVerificacao 
# 16 - Status
# ok


# 17 - Chave1
# 18 - Chave2
# 19 - Chave3
# 20 - Chave4
# 21 - Chave5
# 22 - Chave6
# 23 - Chave7
# 24 - Chave8

# 25 - composicao 
# 26 - Medidas 

# 27 - tags 

listaNormalizadora = []

for i in range(1, len(listaTroc)): 

    roupa = [] 

    #idarquivo
    roupa.append(i - 1) 
    #idLoja
    roupa.append(listaTroc[i][0])
    #Loja
    roupa.append("TROC") 
    # link
    roupa.append(listaTroc[i][1]) 
    # marca
    roupa.append(listaTroc[i][2]) 
    # nomedaPeca
    roupa.append(listaTroc[i][3]) 
    # precoComDesconto
    roupa.append(listaTroc[i][4]) 
    # precoSemDesconto
    roupa.append(listaTroc[i][5]) 
    # Descricao
    roupa.append(listaTroc[i][6]) 
    # Fotos
    roupa.append(listaTroc[i][7]) 
    # tamanho
    roupa.append(listaTroc[i][8]) 
    # condicao
    roupa.append(listaTroc[i][9]) 
    # Cores
    roupa.append(listaTroc[i][10]) 
    # Disponivel
    roupa.append(listaTroc[i][11]) 
    # data
    roupa.append(listaTroc[i][12]) 
    # CodigoDeVerificacao 
    roupa.append(listaTroc[i][13]) 
    # Status
    roupa.append(listaTroc[i][14]) 
    # c1
    roupa.append(listaTroc[i][15]) 
    # c2
    roupa.append(listaTroc[i][16]) 
    # c3
    roupa.append(listaTroc[i][17]) 
    # c4
    roupa.append(listaTroc[i][18]) 
    # c5
    roupa.append(listaTroc[i][19]) 
    # c6
    roupa.append(listaTroc[i][20]) 
    # c7
    roupa.append(listaTroc[i][21]) 
    # c8
    roupa.append(listaTroc[i][22]) 
    # composicao
    roupa.append("") 
    # medidas 
    roupa.append("") 
    # tags
    roupa.append("") 

    listaNormalizadora.append(roupa)

qtdListaNormalizadora = len(listaNormalizadora)

for i in range(1, len(listaRepassa)): 

    roupa = [] 

    #idarquivo
    roupa.append(i - 1 + qtdListaNormalizadora) 
    #idLoja
    roupa.append(listaRepassa[i][0])
    #Loja
    roupa.append("REPASSA") 
    # link
    roupa.append(listaRepassa[i][1]) 
    # marca
    roupa.append(listaRepassa[i][2]) 
    # nomedaPeca
    roupa.append(listaRepassa[i][3]) 
    # precoComDesconto
    roupa.append(listaRepassa[i][5]) 
    # precoSemDesconto
    roupa.append(listaRepassa[i][6]) 
    # Descricao
    roupa.append(listaRepassa[i][7]) 
    # Fotos
    roupa.append(listaRepassa[i][8]) 
    # tamanho
    roupa.append(listaRepassa[i][9]) 
    # condicao
    roupa.append(listaRepassa[i][10]) 
    # Cores
    roupa.append(listaRepassa[i][11]) 
    # Disponivel
    roupa.append(listaRepassa[i][14]) 
    # data
    roupa.append(listaRepassa[i][15]) 
    # CodigoDeVerificacao 
    roupa.append(listaRepassa[i][16]) 
    # Status
    roupa.append(listaRepassa[i][17]) 
    # c1
    roupa.append(listaRepassa[i][18]) 
    # c2
    roupa.append(listaRepassa[i][19]) 
    # c3
    roupa.append(listaRepassa[i][20]) 
    # c4
    roupa.append(listaRepassa[i][21]) 
    # c5
    roupa.append(listaRepassa[i][22]) 
    # c6
    roupa.append(listaRepassa[i][23]) 
    # c7
    roupa.append(listaRepassa[i][24]) 
    # c8
    roupa.append(listaRepassa[i][25]) 
    # composicao
    roupa.append(listaRepassa[i][12]) 
    # medidas 
    roupa.append(listaRepassa[i][13]) 
    # tags
    roupa.append(listaRepassa[i][4]) 

    listaNormalizadora.append(roupa)

qtdListaNormalizadora = len(listaNormalizadora)

for i in range(1, len(listaGarimpario)): 

    roupa = [] 

    #idarquivo
    roupa.append(i  - 1 + qtdListaNormalizadora)
    #idLoja
    roupa.append(listaGarimpario[i][0])
    #Loja
    roupa.append("GARIMPARIO") 
    # link
    roupa.append(listaGarimpario[i][1]) 
    # marca
    roupa.append(listaGarimpario[i][2]) 
    # nomedaPeca
    roupa.append(listaGarimpario[i][3]) 
    # precoComDesconto
    roupa.append(listaGarimpario[i][4]) 
    # precoSemDesconto
    roupa.append(listaGarimpario[i][5]) 
    # Descricao
    roupa.append(listaGarimpario[i][6]) 
    # Fotos
    roupa.append(listaGarimpario[i][7]) 
    # tamanho
    roupa.append(listaGarimpario[i][8]) 
    # condicao
    roupa.append(listaGarimpario[i][9]) 
    # Cores
    roupa.append(listaGarimpario[i][10]) 
    # Disponivel
    roupa.append(listaGarimpario[i][11]) 
    # data
    roupa.append(listaGarimpario[i][12]) 
    # CodigoDeVerificacao 
    roupa.append(listaGarimpario[i][13]) 
    # Status
    roupa.append(listaGarimpario[i][14]) 

    palavras  = listaGarimpario[i][3].strip().split(" ")

    for j in range(0, 8):
        if j < len(palavras): 
            roupa.append(palavras[j])
        else: 
            roupa.append(' ')

    # composicao
    roupa.append('') 
    # medidas 
    roupa.append('') 
    # tags
    roupa.append(listaGarimpario[i][15]) 

    listaNormalizadora.append(roupa)

# gravar arquivo resultado


arquivoOutput = path + 'Normalizados.csv'

with open(arquivoOutput, 'wb') as csvfile:

    spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)

    spamwriter.writerow(['idArquivo', 'idloja', 'Loja', 'link', 'marca', 'nomeDaPeca', 'precoComDesconto', 'precoSemDesconto', 'Descricao', 'Fotos', 'Tamanho', 'Condicao', 'Cores', 'Disponível', 'Data', 'CodigoVerificacao', 'Status', 'Palavra-Chave1', 'Palavra-Chave2','Palavra-Chave3','Palavra-Chave4','Palavra-Chave5','Palavra-Chave6','Palavra-Chave7', 'Palavra-Chave8', 'Composicao', 'Medidas', 'Tags' ])
    
    for peca in listaNormalizadora:

        spamwriter.writerow(peca)



